const puppeteer = require('puppeteer');
const fs = require('fs');
	
let megasena_url = 'http://www.loterias.caixa.gov.br/wps/portal/loterias/landing/megasena/!ut/p/a1/04_Sj9CPykssy0xPLMnMz0vMAfGjzOLNDH0MPAzcDbwMPI0sDBxNXAOMwrzCjA0sjIEKIoEKnN0dPUzMfQwMDEwsjAw8XZw8XMwtfQ0MPM2I02-AAzgaENIfrh-FqsQ9wNnUwNHfxcnSwBgIDUyhCvA5EawAjxsKckMjDDI9FQE-F4ca/dl5/d5/L2dJQSEvUUt3QS80SmlFL1o2XzYxTDBIMEcwSjBJMjgwQTRFUDJWSlYzMDgz/dl5/d5/L2dBISEvZ0FBIS9nQSEh/pw/Z7_HGK818G0KO6H80AU71KG7J0072/res/id=buscaResultado/c=cacheLevelPage/=/';
let diadesorte_url = 'http://www.loterias.caixa.gov.br/wps/portal/loterias/landing/diadesorte/!ut/p/a1/jc5BDsIgFATQs3gCptICXdKSfpA2ujFWNoaVIdHqwnh-sXFr9c_qJ2-SYYGNLEzxmc7xkW5TvLz_IE6WvCoUwZPwArpTnZWD4SCewTGDlrQtZQ-gVGs401gj6wFw4r8-vpzGr_6BhZmIoocFYUO7toLemqYGz0H1AUsTZ7Cw4X7dj0hu9QIyUWUw/dl5/d5/L2dJQSEvUUt3QS80SmlFL1o2X0hHSzgxOEcwS0c2SzYwQUY4Rkg3TUQzMEcz/dl5/d5/L2dBISEvZ0FBIS9nQSEh/pw/Z7_HGK818G0KO5GE0Q8PTB11800G3/res/id=buscaResultado/c=cacheLevelPage/=/';
let lotofacil_url = 'http://www.loterias.caixa.gov.br/wps/portal/loterias/landinglotofacil/!ut/p/a1/04_Sj9CPykssy0xPLMnMz0vMAfGjzOLNDH0MPAzcDbz8vTxNDRy9_Y2NQ13CDA0sTIEKIoEKnN0dPUzMfQwMDEwsjAw8XZw8XMwtfQ0MPM2I02-AAzgaENIfrh-FqsQ9wBmoxN_FydLAGAgNTKEK8DkRrACPGwpyQyMMMj0VAcySpRM!/dl5/d5/L2dJQSEvUUt3QS80SmlFL1o2XzYxTDBIMEcwSk9KSTUwQUtPMzNVRFYxMDg1/dl5/d5/L2dBISEvZ0FBIS9nQSEh/pw/Z7_61L0H0G0J0VSC0AC4GLFAD2003/res/id=buscaResultado/c=cacheLevelPage/=/';
let quina_url = 'http://www.loterias.caixa.gov.br/wps/portal/loterias/landing/quina/!ut/p/a1/jc69DoIwAATgZ_EJepS2wFgoaUswsojYxXQyTfgbjM9vNS4Oordd8l1yxJGBuNnfw9XfwjL78dmduIikhYFGA0tzSFZ3tG_6FCmP4BxBpaVhWQuA5RRWlUZlxR6w4r89vkTi1_5E3CfRXcUhD6osEAHA32Dr4gtsfFin44Bgdw9WWSwj/dl5/d5/L2dJQSEvUUt3QS80SmlFL1o2XzYxTDBIMEcwSjBJMjgwQTRFUDJWSlYzMDM1/dl5/d5/L2dBISEvZ0FBIS9nQSEh/pw/Z7_HGK818G0K8ULB0QT4MEM8L0086/res/id=buscaResultado/c=cacheLevelPage/=/';
let lotomania_url = 'http://www.loterias.caixa.gov.br/wps/portal/loterias/landing/lotomania/!ut/p/a1/04_Sj9CPykssy0xPLMnMz0vMAfGjzOLNDH0MPAzcDbz8vTxNDRy9_Y2NQ13CDA38jYEKIoEKnN0dPUzMfQwMDEwsjAw8XZw8XMwtfQ0MPM2I02-AAzgaENIfrh-FqsQ9wBmoxN_FydLAGAgNTKEK8DkRrACPGwpyQyMMMj0VAajYsZo!/dl5/d5/L2dBISEvZ0FBIS9nQSEh/pw/Z7_61L0H0G0JGJVA0AKLR5T3K00V0/res/id=buscaResultado/c=cacheLevelPage/=/';
let timemania_url = 'http://www.loterias.caixa.gov.br/wps/portal/loterias/landing/timemania/!ut/p/a1/04_Sj9CPykssy0xPLMnMz0vMAfGjzOLNDH0MPAzcDbz8vTxNDRy9_Y2NQ13CDA1MzIEKIoEKnN0dPUzMfQwMDEwsjAw8XZw8XMwtfQ0MPM2I02-AAzgaENIfrh-FqsQ9wBmoxN_FydLAGAgNTKEK8DkRrACPGwpyQyMMMj0VASrq9qk!/dl5/d5/L2dJQSEvUUt3QS80SmlFL1o2XzYxTDBIMEcwSk9KSTUwQUtPMzNVRFYxMDQ3/dl5/d5/L2dBISEvZ0FBIS9nQSEh/pw/Z7_61L0H0G0JGJVA0AKLR5T3K00M4/res/id=buscaResultado/c=cacheLevelPage/=/';
let duplasena_url = 'http://www.loterias.caixa.gov.br/wps/portal/loterias/landing/duplasena/!ut/p/a1/04_Sj9CPykssy0xPLMnMz0vMAfGjzOLNDH0MPAzcDbwMPI0sDBxNXAOMwrzCjA2cDIAKIoEKnN0dPUzMfQwMDEwsjAw8XZw8XMwtfQ0MPM2I02-AAzgaENIfrh-FqsQ9wNnUwNHfxcnSwBgIDUyhCvA5EawAjxsKckMjDDI9FQGgnyPS/dl5/d5/L2dJQSEvUUt3QS80SmlFL1o2XzYxTDBIMEcwSjBJMjgwQTRFUDJWSlYzMEIw/dl5/d5/L2dBISEvZ0FBIS9nQSEh/pw/Z7_HGK818G0KGSE30Q3I6OOK60006/res/id=buscaResultado/c=cacheLevelPage/=/';
let federal_url = 'http://www.loterias.caixa.gov.br/wps/portal/loterias/landing/federal/!ut/p/a1/04_Sj9CPykssy0xPLMnMz0vMAfGjzOLNDH0MPAzcDbz8vTxNDRy9_Y2NQ13CDA0MzIAKIoEKnN0dPUzMfQwMDEwsjAw8XZw8XMwtfQ0MPM2I02-AAzgaENIfrh-FqsQ9wBmoxN_FydLAGAgNTKEK8DkRrACPGwpyQyMMMj0VAYe29yM!/dl5/d5/L2dJQSEvUUt3QS80SmlFL1o2XzYxTDBIMEcwSk9KSTUwQUtPMzNVRFYxMDA2/dl5/d5/L2dBISEvZ0FBIS9nQSEh/pw/Z7_HGK818G0KG4QF0QLDEU6PK2084/res/id=buscaResultado/c=cacheLevelPage/=/';
let loteca_url = 'http://www.loterias.caixa.gov.br/wps/portal/loterias/landing/loteca/!ut/p/a1/04_Sj9CPykssy0xPLMnMz0vMAfGjzOLNDH0MPAzcDbz8vTxNDRy9_Y2NQ13CDA3cDYEKIoEKnN0dPUzMfQwMDEwsjAw8XZw8XMwtfQ0MPM2I02-AAzgaENIfrh-FqsQ9wBmoxN_FydLAGAgNTKEK8DkRrACPGwpyQyMMMj0VAbNnwlU!/dl5/d5/L2dJQSEvUUt3QS80SmlFL1o2XzYxTDBIMEcwSk9KSTUwQUtPMzNVRFYxMEcx/dl5/d5/L2dBISEvZ0FBIS9nQSEh/pw/Z7_HGK818G0KOCO10AFFGUTGU0004/res/id=buscaResultado/c=cacheLevelPage/=/';
let supersete_url = 'http://www.loterias.caixa.gov.br/wps/portal/loterias/landing/supersete/!ut/p/a1/jc5BDsIgEAXQs3gCPralsISSAFK1XaiVjWFlSLS6MJ5fbNxanVnN5P3kk0AGEsb4TOf4SLcxXt53YCdrPKfcwHOtGvTdfiMK31OgzOCYQWOkLesW-cOXcFpZXYs14Nh_eXwZiV_5AwkTYbSFhcHKdE0FudVKoMiL6gPmKk5gpsP9uhuQ3OIFKJSbBA!!/dl5/d5/L2dJQSEvUUt3QS80SmlFL1o2X0hHSzgxOEcwSzhEQkMwUVBWTjkzS1ExMDA0/dl5/d5/L2dBISEvZ0FBIS9nQSEh/pw/Z7_HGK818G0K85260Q5OIRSC420K6/res/id=buscaResultado/c=cacheLevelPage/=/';

const lotery_games = {
	'megasena': megasena_url,
	'diadesorte': diadesorte_url,
	'lotofacil': lotofacil_url,
	'quina': quina_url,
	'lotomania': lotomania_url,
	'timemania': timemania_url,
	'duplasena': duplasena_url,
	'federal': federal_url,
	'loteca': loteca_url,
	'supersete': supersete_url
}

async function api(query) {
	const browser = await puppeteer.launch();
	const page = await browser.newPage();
	if (lotery_games[query]) {
		await page.goto(lotery_games[query]);
	 	requestData = await page.content();
	} else {
		requestData = {
			'info': 'invalid request!'
		}
	}

  console.log(requestData);
  // fs.writeFile('jogos/megasena/megasena.json', requestData, function (err) {
  // if (err) return console.log(err);
  // console.log('megasena > megasena.json');
	// });
};
	

api('megasena');
